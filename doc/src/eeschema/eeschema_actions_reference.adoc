:experimental:

[[eeschema-actions-reference]]
== Actions reference
Below is a list of every available *action* in the KiCad Schematic Editor: a
command that can be assigned to a hotkey.

////
Note to translators: you do not need to translate this table by hand.

It is generated from KiCad using the Dump Hotkeys button that is shown in the hotkeys editor if you
add the line `HotkeysDumper=1` to your advanced config file (`kicad_advanced` file in the config
directory)
////

=== Schematic Editor

// NOTE: this text between the section header and the table is *required* or
// asciidoctor-web-pdf will not insert page breaks in the table correctly and
// the PDF will be truncated.
The actions below are available in the Schematic Editor. Hotkeys can be assigned
to any of these actions in the **Hotkeys** section of the preferences.

[width="100%",options="header",cols="20%,15%,65%"]
|===
| Action | Default Hotkey | Description
| Align Items to Grid
  |
  | 
| Annotate Schematic...
  |
  | Fill in schematic symbol reference designators
| Annotate Automatically
  |
  | Toggle automatic annotation of new symbols
| Assign Footprints...
  |
  | Run footprint assignment tool
| Clear Net Highlighting
  | kbd:[~]
  | Clear any existing net highlighting
| Export Drawing to Clipboard
  |
  | Export drawing of current sheet to clipboard
| Edit Library Symbol...
  | kbd:[Ctrl+Shift+E]
  | Open the library symbol in the Symbol Editor
| Edit Sheet Page Number...
  |
  | Edit the page number of the current or selected sheet
| Edit Symbol Fields...
  |
  | Bulk-edit fields of all symbols in schematic
| Edit Symbol Library Links...
  |
  | Edit links between schematic and library symbols
| Edit with Symbol Editor
  | kbd:[Ctrl+E]
  | Open the selected symbol in the Symbol Editor
| Export Netlist...
  |
  | Export file containing netlist in one of several formats
| Export Symbols to Library...
  |
  | Add symbols used in schematic to an existing symbol library
(does not remove other symbols from this library)
| Export Symbols to New Library...
  |
  | Create a new symbol library using the symbols used in the schematic
(if the library already exists it will be replaced)
| Generate Bill of Materials...
  |
  | Generate a bill of materials for the current schematic
| Generate Bill of Materials (External)...
  |
  | Generate a bill of materials for the current schematic using external generator
| Generate Legacy Bill of Materials...
  |
  | Generate a bill of materials for the current schematic (Legacy Generator)
| Highlight Net
  | kbd:[`]
  | Highlight net under cursor
| Highlight Nets
  |
  | Highlight wires and pins of a net
| Import Footprint Assignments...
  |
  | Import symbol footprint assignments from .cmp file created by board editor
| Import Graphics...
  | kbd:[Ctrl+Shift+F]
  | Import 2D drawing file
| Increment Annotations From...
  |
  | Increment a subset of reference designators starting at a particular symbol
| Line Mode for Wires and Buses
  |
  | Constrain drawing and dragging to horizontal, vertical, or 45-degree angle motions
| Line Mode for Wires and Buses
  |
  | Draw and drag at any angle
| Line Mode for Wires and Buses
  | kbd:[Shift+Space]
  | Switch to next line mode
| Line Mode for Wires and Buses
  |
  | Constrain drawing and dragging to horizontal or vertical motions
| Mark items excluded from simulation
  |
  | Draw 'X's over items which have been excluded from simulation
| Next Symbol Unit
  |
  | Open the next unit of the symbol
| Previous Symbol Unit
  |
  | Open the previous unit of the symbol
| Remap Legacy Library Symbols...
  |
  | Remap library symbol references in legacy schematics to the symbol library table
| Repair Schematic
  |
  | Run various diagnostics and attempt to repair schematic
| Rescue Symbols...
  |
  | Find old symbols in project and rename/rescue them
| Save Current Sheet Copy As...
  |
  | Save a copy of the current sheet to another location or name
| Schematic Setup...
  |
  | Edit schematic setup including annotation styles and electrical rules
| Select on PCB
  |
  | Select corresponding items in PCB editor
| Do not Populate
  |
  | Set the do not populate attribute
| Exclude from Bill of Materials
  |
  | Set the exclude from bill of materials attribute
| Exclude from Board
  |
  | Set the exclude from board attribute
| Exclude from Simulation
  |
  | Set the exclude from simulation attribute
| Show Directive Labels
  |
  | 
| Show ERC Errors
  |
  | Show markers for electrical rules checker errors
| Show ERC Exclusions
  |
  | Show markers for excluded electrical rules checker violations
| Show ERC Warnings
  |
  | Show markers for electrical rules checker warnings
| Show Hidden Fields
  |
  | 
| Show Hidden Pins
  |
  | 
| Net Navigator
  |
  | Show/hide the net navigator
| Show OP Currents
  |
  | Show operating point current data from simulation
| Show OP Voltages
  |
  | Show operating point voltage data from simulation
| Switch to PCB Editor
  |
  | Open PCB in board editor
| Simulator
  |
  | Show simulation window for running SPICE or IBIS simulations.
| Show Pin Alternate Icons
  |
  | Show indicator icons for pins with alternate modes
| Hierarchy Navigator
  | kbd:[Ctrl+H]
  | Show/hide the schematic sheet hierarchy navigator
| Symbol Checker
  |
  | Show the symbol checker window
| Compare Symbol with Library
  |
  | Show differences between schematic symbol and its library equivalent
| Electrical Rules Checker
  |
  | Show the electrical rules checker window
| Show Bus Syntax Help
  |
  | 
| Decrement Primary
  |
  | Decrement the primary field of the selected item(s)
| Decrement Secondary
  |
  | Decrement the secondary field of the selected item(s)
| Increment
  |
  | Increment the selected item(s)
| Increment Primary
  |
  | Increment the primary field of the selected item(s)
| Increment Secondary
  |
  | Increment the secondary field of the selected item(s)
| Close Outline
  |
  | Close the in-progress outline
| Delete Last Point
  |
  | Delete the last point added to the current item
| Draw Arcs
  |
  | 
| Draw Bezier Curve
  |
  | 
| Draw Circles
  |
  | 
| Draw Rectangles
  |
  | 
| Draw Rule Areas
  |
  | 
| Draw Hierarchical Sheets
  | kbd:[S]
  | 
| Draw Sheet from Design Block
  |
  | Copy design block into project as a sheet on current sheet
| Draw Sheet from File
  |
  | Copy sheet into project and draw on current sheet
| Draw Tables
  |
  | 
| Draw Text Boxes
  |
  | 
| Import Sheet
  |
  | Import sheet into project
| Place Wire to Bus Entries
  | kbd:[Z]
  | 
| Place Directive Labels
  |
  | 
| Place Design Block
  | kbd:[Shift+B]
  | Add selected design block to current sheet
| Place Global Labels
  | kbd:[Ctrl+L]
  | 
| Place Hierarchical Labels
  | kbd:[H]
  | 
| Place Images
  |
  | 
| Place Junctions
  | kbd:[J]
  | 
| Place Net Labels
  | kbd:[L]
  | 
| Place Next Symbol Unit
  |
  | Place the next unit of the current symbol that is missing from the schematic
| Place No Connect Flags
  | kbd:[Q]
  | 
| Place Power Symbols
  | kbd:[P]
  | 
| Draw Text
  | kbd:[T]
  | 
| Place Sheet Pins
  |
  | 
| Place Symbols
  | kbd:[A]
  | 
| Sync Sheet Pins
  |
  | Synchronize sheet pins and hierarchical labels
| Sync Sheet Pins
  |
  | Synchronize sheet pins and hierarchical labels
| Draw Buses
  | kbd:[B]
  | 
| Draw Lines
  | kbd:[I]
  | 
| Draw Wires
  | kbd:[W]
  | 
| Switch Segment Posture
  | kbd:[/]
  | Switches posture of the current segment.
| Undo Last Segment
  | kbd:[Back]
  | Walks the current line back one segment.
| Unfold from Bus
  | kbd:[C]
  | Break a wire out of a bus
| Assign Netclass...
  |
  | Assign a netclass to nets matching a pattern
| Autoplace Fields
  | kbd:[O]
  | Runs the automatic placement algorithm on the symbol's (or sheet's) fields
| Break
  |
  | Divide into connected segments
| Change Symbol...
  |
  | Assign a different symbol from the library
| Change Symbols...
  |
  | Assign different symbols from the library
| Cleanup Sheet Pins
  |
  | Delete unreferenced sheet pins
| Edit Footprint…
  | kbd:[F]
  | 
| Edit Reference Designator...
  | kbd:[U]
  | 
| Edit Text & Graphics Properties...
  |
  | Edit text and graphics properties globally across schematic
| Edit Value…
  | kbd:[V]
  | 
| Mirror Horizontally
  | kbd:[X]
  | Flips selected item(s) from left to right
| Mirror Vertically
  | kbd:[Y]
  | Flips selected item(s) from top to bottom
| Pin Table...
  |
  | Displays pin table for bulk editing of pins
| Properties…
  | kbd:[E]
  | 
| Repeat Last Item
  | kbd:[Ins]
  | Duplicates the last drawn item
| Rotate Counterclockwise
  | kbd:[R]
  | 
| Rotate Clockwise
  |
  | 
| De Morgan Alternate
  |
  | Switch to alternate De Morgan representation
| De Morgan Standard
  |
  | Switch to standard De Morgan representation
| Slice
  |
  | Divide into unconnected segments
| Swap
  | kbd:[Alt+S]
  | Swap positions of selected items
| Symbol Properties...
  |
  | 
| Change to Directive Label
  |
  | Change existing item to a directive label
| Change to Global Label
  |
  | Change existing item to a global label
| Change to Hierarchical Label
  |
  | Change existing item to a hierarchical label
| Change to Label
  |
  | Change existing item to a label
| Change to Text
  |
  | Change existing item to a text comment
| Change to Text Box
  |
  | Change existing item to a text box
| De Morgan Conversion
  |
  | Switch between De Morgan representations
| Update Symbol...
  |
  | Update symbol to include any changes from the library
| Update Symbols from Library...
  |
  | Update symbols to include any changes from the library
| Drag
  | kbd:[G]
  | Move items while keeping their connections
| Move
  | kbd:[M]
  | 
| Select Connection
  | kbd:[Ctrl+4]
  | Select a complete connection
| Select Node
  | kbd:[Alt+3]
  | Select a connection item under the cursor
| Navigate Back
  | kbd:[Alt+Left]
  | Move backward in sheet navigation history
| Change Sheet
  |
  | Change to provided sheet's contents in the schematic editor
| Enter Sheet
  |
  | Display the selected sheet's contents in the schematic editor
| Navigate Forward
  | kbd:[Alt+Right]
  | Move forward in sheet navigation history
| Leave Sheet
  | kbd:[Alt+Back]
  | Display the parent sheet in the schematic editor
| Next Sheet
  | kbd:[PgDn]
  | Move to next sheet by number
| Previous Sheet
  | kbd:[PgUp]
  | Move to previous sheet by number
| Navigate Up
  | kbd:[Alt+Up]
  | Navigate up one sheet in the hierarchy
| Push Pin Length
  |
  | Copy pin length to other pins in symbol
| Push Pin Name Size
  |
  | Copy pin name size to other pins in symbol
| Push Pin Number Size
  |
  | Copy pin number size to other pins in symbol
| Create Corner
  |
  | 
| Remove Corner
  |
  | 
| Properties…
  |
  | Edit properies of design block
| Delete Design Block
  |
  | Remove the selected design block from its library
| Save Selection as Design Block...
  |
  | Create a new design block from the current selection
| Save Current Sheet as Design Block...
  |
  | Create a new design block from the current sheet
| Design Blocks
  |
  | Show/hide design blocks library
| User-defined Signals...
  |
  | Add, edit or delete user-defined simulation signals
| New Analysis Tab...
  | kbd:[Ctrl+N]
  | Create a new tab containing a simulation analysis
| Open Workbook...
  | kbd:[Ctrl+O]
  | Open a saved set of analysis tabs and settings
| Probe Schematic...
  | kbd:[P]
  | Add a simulator probe
| Run Simulation
  | kbd:[R]
  | 
| Save Workbook
  | kbd:[Ctrl+S]
  | Save the current set of analysis tabs and settings
| Save Workbook As...
  | kbd:[Ctrl+Shift+S]
  | Save the current set of analysis tabs and settings to another location
| Show SPICE Netlist
  |
  | 
| Edit Analysis Tab...
  |
  | Edit the current analysis tab's SPICE command and plot setup
| Stop Simulation
  |
  | 
| Add Tuned Value...
  | kbd:[T]
  | Select a value to be tuned
| Export Current Plot as CSV...
  |
  | 
| Export Current Plot as PNG...
  |
  | 
| Export Current Plot to Schematic
  |
  | 
| Export Current Plot to Clipboard
  |
  | 
| Dark Mode Plots
  |
  | Draw plots with a black background
| Dotted Current/Phase
  |
  | Draw secondary signal trace (current or phase) with a dotted line
| Show Legend
  |
  | 
| Draw Lines
  |
  | Draw connected graphic lines
| Draw Polygons
  |
  | 
| Draw Text Boxes
  |
  | 
| Move Symbol Anchor
  |
  | 
| Draw Pins
  | kbd:[P]
  | 
| Draw Text
  |
  | 
| Add Symbol to Schematic
  |
  | Add the current symbol to the schematic
| Copy
  |
  | 
| Cut
  |
  | 
| Delete Symbol
  |
  | Remove the selected symbol from its library
| Derive from Existing Symbol...
  |
  | Create a new symbol, derived from an existing symbol
| Duplicate Symbol
  |
  | 
| Edit Symbol
  |
  | Show selected symbol on editor canvas
| Export Symbol as SVG…
  |
  | Create SVG file from the current symbol
| Export View as PNG…
  |
  | Create PNG file from the current view
| Import Symbol...
  |
  | Import a symbol to the current library
| New Symbol...
  | kbd:[Ctrl+N]
  | Create a new symbol in an existing library
| Paste Symbol
  |
  | 
| Rename Symbol...
  |
  | 
| Save Library As...
  | kbd:[Ctrl+Shift+S]
  | Save the current library to a new file
| Save As…
  |
  | Save the current symbol to a different library or name
| Save Copy As...
  |
  | Save a copy of the current symbol to a different library or name
| Set Unit Display Name...
  |
  | Set the display name for a particular unit in a multi-unit symbol
| Show Pin Electrical Types
  |
  | Annotate pins with their electrical types
| Show Hidden Fields
  |
  | 
| Show Hidden Pins
  |
  | 
| Show Pin Numbers
  |
  | Annotate pins with their numbers
| Synchronized Pins Mode
  |
  | Synchronized Pins Mode
When enabled propagates all changes (except pin numbers) to other units.
Enabled by default for multiunit parts with interchangeable units.
| Update Symbol Fields...
  |
  | Update symbol to match changes made in parent symbol
|===

=== Common

// NOTE: this text between the section header and the table is *required* or
// asciidoctor-web-pdf will not insert page breaks in the table correctly and
// the PDF will be truncated.
The actions below are available across KiCad, including in the Schematic Editor.
Hotkeys can be assigned to any of these actions in the **Hotkeys** section of
the preferences.

[width="100%",options="header",cols="20%,15%,65%"]
|===
| Action | Default Hotkey | Description
| Refresh Plugins
  |
  | Reload all python plugins and refresh plugin menus
| Exclude Marker
  |
  | Mark current violation in Checker window as an exclusion
| Next Marker
  |
  | 
| Previous Marker
  |
  | 
| Add Library…
  |
  | Add an existing library folder
| Center Justify
  |
  | Center-justify fields and text items
| Pan to Center Selected Objects
  |
  | 
| Collapse All
  |
  | 
| Click
  | kbd:[Return]
  | Performs left mouse button click
| Double-click
  | kbd:[End]
  | Performs left mouse button double-click
| Cursor Down
  | kbd:[Down]
  | 
| Cursor Down Fast
  | kbd:[Ctrl+Down]
  | 
| Cursor Left
  | kbd:[Left]
  | 
| Cursor Left Fast
  | kbd:[Ctrl+Left]
  | 
| Cursor Right
  | kbd:[Right]
  | 
| Cursor Right Fast
  | kbd:[Ctrl+Right]
  | 
| Cursor Up
  | kbd:[Up]
  | 
| Cursor Up Fast
  | kbd:[Ctrl+Up]
  | 
| Grid Origin...
  |
  | Set the grid origin point
| Edit Grids...
  |
  | Edit grid definitions
| Expand All
  |
  | 
| Switch to Fast Grid 1
  | kbd:[Alt+1]
  | 
| Switch to Fast Grid 2
  | kbd:[Alt+2]
  | 
| Cycle Fast Grid
  | kbd:[Alt+4]
  | 
| Switch to Next Grid
  | kbd:[N]
  | 
| Switch to Previous Grid
  | kbd:[Shift+N]
  | 
| Reset Grid Origin
  |
  | 
| Grid Origin
  |
  | Place the grid origin point
| Hide Library Tree
  |
  | 
| Inactive Layer View Mode
  |
  | Toggle inactive layers between normal and dimmed
| Inactive Layer View Mode (3-state)
  | kbd:[H]
  | Cycle inactive layers between normal, dimmed, and hidden
| Inches
  |
  | 
| Left Justify
  |
  | Left-justify fields and text items
| Focus Library Tree Search Field
  | kbd:[Ctrl+L]
  | 
| Snap to Objects on the Active Layer Only
  |
  | Enables snapping to objects on the active layer only
| Snap to Objects on All Layers
  |
  | Enables snapping to objects on all visible layers
| Toggle Snapping Between Active and All Layers
  | kbd:[Shift+S]
  | Toggles between snapping on all visible layers and only the active area
| Millimeters
  |
  | 
| Mils
  |
  | 
| New...
  | kbd:[Ctrl+N]
  | Create a new document in the editor
| New Library…
  |
  | Create a new library folder
| Open...
  | kbd:[Ctrl+O]
  | Open existing document
| Open in file explorer...
  |
  | Open a library file with system file explorer
| Edit in a Text Editor...
  |
  | Open a library file with a text editor
| Page Settings...
  |
  | Settings for paper size and title block info
| Pan Down
  | kbd:[Shift+Down]
  | 
| Pan Left
  | kbd:[Shift+Left]
  | 
| Pan Right
  | kbd:[Shift+Right]
  | 
| Pan Up
  | kbd:[Shift+Up]
  | 
| Pin Library
  |
  | Keep the library at the top of the list
| Plot...
  |
  | 
| Print...
  | kbd:[Ctrl+P]
  | 
| Quit
  |
  | Close the current editor
| Redo Last Zoom
  |
  | Return zoom to level prior to last zoom undo
| Reset Local Coordinates
  | kbd:[Space]
  | 
| Revert
  |
  | Throw away changes
| Right Justify
  |
  | Right-justify fields and text items
| Save
  | kbd:[Ctrl+S]
  | Save changes
| Save All
  |
  | Save all changes
| Save As…
  | kbd:[Ctrl+Shift+S]
  | Save current document to another location
| Save a Copy...
  |
  | Save a copy of the current document to another location
| Select Columns...
  |
  | 
| 3D Viewer
  | kbd:[Alt+3]
  | Show 3D viewer window
| Show Context Menu
  |
  | Perform the right-mouse-button action
| Show Datasheet
  | kbd:[D]
  | Open the datasheet in a browser
| Footprint Library Browser
  |
  | 
| Footprint Editor
  |
  | Create, delete and edit board footprints
| Library Tree
  |
  | 
| Switch to Project Manager
  |
  | Show project window
| Properties
  |
  | Show/hide the properties manager
| Symbol Library Browser
  |
  | 
| Symbol Editor
  |
  | Create, delete and edit schematic symbols
| Draw Bounding Boxes
  |
  | 
| Always Show Crosshairs
  | kbd:[Ctrl+Shift+X]
  | Display crosshairs even when not drawing objects
| Full-Window Crosshairs
  |
  | Switch display of full-window crosshairs
| Show Grid
  |
  | Display background grid in the edit window
| Grid Overrides
  | kbd:[Ctrl+Shift+G]
  | Enables item-specific grids that override the current grid
| Polar Coordinates
  |
  | Switch between polar and cartesian coordinate systems
| Switch units
  | kbd:[Ctrl+U]
  | Switch between imperial and metric units
| Undo Last Zoom
  |
  | Return zoom to level prior to last zoom action
| Unpin Library
  |
  | No longer keep the library at the top of the list
| Update PCB from Schematic…
  | kbd:[F8]
  | Update PCB with changes made to schematic
| Update Schematic from PCB...
  |
  | Update schematic with changes made to PCB
| Center on Cursor
  | kbd:[F4]
  | 
| Zoom to Objects
  | kbd:[Ctrl+Home]
  | 
| Zoom to Fit
  | kbd:[Home]
  | 
| Zoom to Selected Objects
  |
  | 
| Zoom In at Cursor
  | kbd:[F1]
  | 
| Zoom In
  |
  | 
| Zoom In Horizontally
  |
  | Zoom in horizontally the plot area
| Zoom In Vertically
  |
  | Zoom in vertically the plot area
| Zoom Out at Cursor
  | kbd:[F2]
  | 
| Zoom Out
  |
  | 
| Zoom Out Horizontally
  |
  | Zoom out horizontally the plot area
| Zoom Out Vertically
  |
  | Zoom out vertically the plot area
| Refresh
  | kbd:[F5]
  | 
| Zoom to Selection
  | kbd:[Ctrl+F5]
  | 
| Embedded Files
  |
  | Manage embedded files
| Extract File
  |
  | Extract an embedded file
| Remove File
  |
  | Remove an embedded file
| Cancel
  |
  | Cancel current tool
| Copy
  | kbd:[Ctrl+C]
  | Copy selected item(s) to clipboard
| Copy as Text
  | kbd:[Ctrl+Shift+C]
  | Copy selected item(s) to clipboard as text
| Cut
  | kbd:[Ctrl+X]
  | Cut selected item(s) to clipboard
| Cycle Arc Editing Mode
  | kbd:[Ctrl+Space]
  | Switch to a different method of editing arcs
| Delete
  | kbd:[Del]
  | Delete selected item(s)
| Interactive Delete Tool
  |
  | Delete clicked items
| Duplicate
  | kbd:[Ctrl+D]
  | Duplicates the selected item(s)
| Find
  | kbd:[Ctrl+F]
  | 
| Find and Replace
  | kbd:[Ctrl+Alt+F]
  | 
| Find Next
  | kbd:[F3]
  | 
| Find Next Marker
  | kbd:[Ctrl+Shift+F3]
  | 
| Find Previous
  | kbd:[Shift+F3]
  | 
| Finish
  | kbd:[End]
  | Finish current tool
| Measure Tool
  | kbd:[Ctrl+Shift+M]
  | Interactively measure distance between points
| Paste
  | kbd:[Ctrl+V]
  | Paste item(s) from clipboard
| Paste Special...
  |
  | Paste item(s) from clipboard with options
| Redo
  | kbd:[Ctrl+Y]
  | 
| Replace All
  |
  | 
| Replace and Find Next
  |
  | 
| Search
  | kbd:[Ctrl+G]
  | Show/hide the search panel
| Select All
  | kbd:[Ctrl+A]
  | Select all items on screen
| Undo
  | kbd:[Ctrl+Z]
  | 
| Unselect All
  | kbd:[Ctrl+Shift+A]
  | Unselect all items on screen
| Select Row(s)
  |
  | Select complete row(s) containing the current selected cell(s)
| Select Column(s)
  |
  | Select complete column(s) containing the current selected cell(s)
| Select Table
  |
  | Select parent table of selected cell(s)
| Select item(s)
  |
  | 
| About KiCad
  |
  | 
| Configure Paths…
  |
  | Edit path configuration environment variables
| Donate
  |
  | Open "Donate to KiCad" in a web browser
| Get Involved
  |
  | Open "Contribute to KiCad" in a web browser
| Getting Started with KiCad
  |
  | Open “Getting Started in KiCad” guide for beginners
| Help
  |
  | Open product documentation in a web browser
| List Hotkeys...
  | kbd:[Ctrl+F1]
  | Displays current hotkeys table and corresponding commands
| Preferences...
  | kbd:[Ctrl+,]
  | Show preferences for all open tools
| Report Bug
  |
  | Report a problem with KiCad
| Manage Design Block Libraries...
  |
  | Edit the global and project design block library lists
| Manage Footprint Libraries...
  |
  | Edit the global and project footprint library lists
| Manage Symbol Libraries…
  |
  | Edit the global and project symbol library lists
| Add Column After
  |
  | Insert a new table column after the selected cell(s)
| Add Column Before
  |
  | Insert a new table column before the selected cell(s)
| Add Row Above
  |
  | Insert a new table row above the selected cell(s)
| Add Row Below
  |
  | Insert a new table row below the selected cell(s)
| Delete Column(s)
  |
  | Delete columns containing the currently selected cell(s)
| Delete Row(s)
  |
  | Delete rows containing the currently selected cell(s)
| Merge Cells
  |
  | Turn selected table cells into a single cell
| Unmerge Cells
  |
  | Turn merged table cells back into separate cells.
|===
